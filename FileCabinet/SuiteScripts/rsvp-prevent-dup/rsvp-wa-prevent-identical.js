define(["./rsvp-prevent-dup", "N/format", "N/runtime", "N/log"], function (dup, f, run, log) {

    /**
     * Workflow Action entry point for identical RSVP detection
     *
     * @exports rsvp/prevent-identical/wa
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope Public
     * @NScriptType WorkflowActionScript
     *
     * @requires rsvp/prevent-dup
     * @requires N/format
     * @requires N/runtime
     * @requires N/log
     */
    var exports = {};

    /**
     * <code>onAction</code> event handler
     *
     * @governance 15
     *
     * @param context
     *        {Object}
     * @param context.newRecord
     *        {Record} The new record with all changes. <code>save()</code> is not
     *        permitted.
     * @param context.oldRecord
     *        {Record} The old record with all changes. <code>save()</code> is not
     *        permitted.
     * @param context.form
     *        {serverWidget.Form} The record form loaded in the UI
     * @param context.type
     *        {String} The event type, such as create, edit, view, delete, etc
     * @param context.workflowId
     *        {String} The internal ID of the workflow that invoked this script
     *
     * @return {String} Value to be set in "Has Identical RSVP" checkbox: <code>"T"</code> if there are duplicate
     *      RSVPs; <code>"F"</code> otherwise
     *
     * @static
     * @function onAction
     */
    function onAction(context) {
        log.audit({
            title: "RSVP",
            details: "Attempting to locate identical RSVP..."
        });

        return dup.hasIdentical(rawData(context.newRecord, run.getCurrentScript())) ? "T" : "F";
    }

    /**
     * Reads required contextual data
     *
     * @governance 0
     *
     * @param {r.Record} record - The Case record being created
     * @param {run.Script} script - The currently executing Script
     *
     * @returns {dup.RecordData} Raw data required for duplicate detection
     *
     * @private
     * @function rawData
     */
    function rawData(record, script) {
        var data = {
            comments: record.getValue({fieldId: "custevent_comments_questions"}),
            email: record.getValue({fieldId: "email"}),
            isAttending: record.getValue({fieldId: "custevent_will_be_attending"}),
            isRemote: record.getValue({fieldId: "custevent_request_attend_remotely"}),
            meetingDate: f.format({
                value: record.getValue({fieldId: "custevent_meeting_date"}),
                type: f.Type.DATE
            }),
            memberType: record.getValue({fieldId: "custevent_member_type"}),
            searchId: script.getParameter({name: "custscript_rsvp_identical_search"})
        };
        log.debug({
            title: "RSVP",
            details: data
        });

        return data;
    }

    exports.onAction = onAction;
    return exports;
});
