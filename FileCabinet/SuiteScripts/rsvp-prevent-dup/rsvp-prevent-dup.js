define(["N/search", "N/error", "N/log"], function (s, e, log) {

    /**
     * Determines whether an RSVP Case record already exists for the next RMNSUG meeting.
     * There are two ways an RSVP can "already exist":
     * <dl>
     *     <dt>Identical</dt>
     *     <dd>The fields on the RSVP are exactly the same as an existing record</dd>
     *     <dt>Duplicate</dt>
     *     <dd>The RSVP is for the same company/member, but varies by any of the following fields:
     *       <ul>
     *           <li>I would like to attend remotely</li>
     *           <li>Member Type</li>
     *           <li>Comments or Questions</li>
     *       </ul>
     *     </dd>
     * </dl>
     *
     * When the RSVP is considered Identical, we reject the creation of the Case record.
     *
     * Otherwise, we return the appropriate value to check/uncheck the "Has Duplicate RSVP" box
     *
     * @exports rsvp/prevent-dup
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope Public
     *
     * @requires N/search
     * @requires N/error
     * @requires N/log
     */
    var exports = {};

    /**
     * @typedef {Object} RecordData
     * @property {String} searchId - Internal ID of the Saved Search to execute
     * @property {String} email - The email address of the Company on the Case
     */

    /**
     * Determines whether the Case being created is a duplicate
     *
     * @governance 15
     *
     * @param {RecordData} data - Raw data required for duplicate detection
     *
     * @returns {Boolean} <code>true</code> if there are duplicate RSVPs; <code>false</code> otherwise
     *
     * @static
     * @function hasDuplicates
     */
    function hasDuplicates(data) {
        return !!duplicateCount(validData(data));
    }

    /**
     * Determines whether the Case being created has an identical
     *
     * @governance 15
     *
     * @param {RecordData} data - Raw data required for duplicate detection
     *
     * @returns {Boolean} <code>true</code> if there are duplicate RSVPs; <code>false</code> otherwise
     *
     * @static
     * @function hasIdentical
     */
    function hasIdentical(data) {
        return !!identicalCount(validData(data));
    }

    /**
     * Ensures the given raw data is valid.  Raw data must contain both a Saved Search ID obtained from the
     * Script Parameter and an email address obtained from the record being created
     *
     * @governance 0
     *
     * @param {RecordData} data - Raw data required for duplicate detection
     *
     * @returns {RecordData} Valid data required for duplicate detection
     *
     * @throws {e.SuiteScriptError} when no Saved Search ID or Email Address is found in the raw data
     *
     * @private
     * @function validData
     */
    function validData(data) {
        if (!(data && data.searchId && data.email && data.meetingDate)) {
            log.audit({
                title: "RSVP",
                details: "Search ID or Email not provided; exiting..."
            });
            throw e.create({
                name: "RSVP_DUP_DETECTION_ERROR",
                message: "No Saved Search or Email Address was found.",
                notifyOff: true
            });
        }
        return data;
    }

    /**
     * Counts the number of duplicate RSVP cases for the given email address returned by the given Saved Search
     *
     * @governance 15
     *
     * @param {RecordData} data - Valid data required for duplicate detection
     *
     * @returns {Number} The number of duplicate RSVP Cases
     *
     * @private
     * @function duplicateCount
     */
    function duplicateCount(data) {
        var search = s.load({
            id: data.searchId
        });
        search.filterExpression = search.filterExpression.concat([
            "AND",
            ["email", s.Operator.IS, data.email], "AND",
            ["custevent_member_type", s.Operator.ANYOF, [data.memberType]], "AND",
            ["custevent_meeting_date", s.Operator.ON, data.meetingDate]
        ]);

        var dupCount = search.run()
            .getRange({start: 0, end: 1})[0]
            .getValue({name: "internalid", summary: s.Summary.COUNT});
        log.debug({
            title: "RSVP",
            details: "Duplicate Count = " + dupCount
        });

        return parseInt(dupCount, 10);
    }

    /**
     * Counts the number of identical RSVP cases for the given email address returned by the given Saved Search
     *
     * @governance 15
     *
     * @param {RecordData} data - Valid data required for duplicate detection
     *
     * @returns {Number} The number of duplicate RSVP Cases
     *
     * @private
     * @function duplicateCount
     */
    function identicalCount(data) {
        var search = s.load({
            id: data.searchId
        });
        search.filterExpression = search.filterExpression.concat([
            "AND",
            ["email", s.Operator.IS, data.email], "AND",
            ["custevent_meeting_date", s.Operator.ON, data.meetingDate], "AND",
            commentFilter(data), "AND",
            ["custevent_will_be_attending", s.Operator.IS, data.isAttending], "AND",
            ["custevent_member_type", s.Operator.ANYOF, [data.memberType]], "AND",
            ["custevent_request_attend_remotely", s.Operator.IS, data.isRemote]
        ]);

        var identicalCount = search.run()
            .getRange({start: 0, end: 1})[0]
            .getValue({name: "internalid", summary: s.Summary.COUNT});
        log.debug({
            title: "RSVP",
            details: "Identical Count = " + identicalCount
        });

        return parseInt(identicalCount, 10);
    }

    /**
     * Builds an appropriate filter expression for the Comment field based on the given data
     *
     * @governance 0
     *
     * @param {RecordData} data - Valid data required for duplicate detection
     *
     * @returns {String[]} Filter Expression for the Comment field
     *
     * @private
     * @function commentFilter
     */
    function commentFilter(data) {
        return (data.comments) ?
            ["custevent_comments_questions", s.Operator.IS, data.comments] :
            ["custevent_comments_questions", s.Operator.ISEMPTY, ""];
    }

    exports.hasDuplicates = hasDuplicates;
    exports.hasIdentical = hasIdentical;
    return exports;
});
