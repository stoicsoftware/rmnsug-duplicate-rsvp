## RMNSUG Duplicate RSVP Prevention

Prevents creation of duplicate RSVPs within NetSuite for RMNSUG

#### Directory structure
```
/
  FileCabinet/SuiteScripts/
    prevent-dup-rsvp/ - Project module folder
```
